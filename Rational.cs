﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartCExam
{
    class Rational
    {
        private int P { get; set; }
        private int Q { get; set; }
        public int Our_Number { get; }

        public Rational(int p, int q)
        {
            P = p;
            if (q <= 0)
            {
                P = 0;
                Q = 0;
                Our_Number = 0;
            }
            else
            {
                Q = q;
            }
        }
        public bool GreaterThan (Rational number)
        {
            if (P * number.Q > Q * number.P)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool Equals (Rational number)
        {
            if (P * number.Q == Q * number.P)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Rational Plus (Rational number)
        {
            return new Rational((P * number.Q) + (Q * number.P), Q * number.Q);
        }
        public Rational Minus (Rational number)
        {
            return new Rational((P * number.Q) - (Q * number.P), Q * number.Q);
        }
        public Rational Multyply (Rational number)
        {
            return new Rational(P * number.P, Q * number.Q);
        }
        public int GetNumerator()
        {
            return P;
        }
        public int GetDenominator()
        {
            return Q;
        }

        public override string ToString()
        {
            if (Our_Number == 0)
            {
                return Our_Number.ToString();
            }
            else
            {
                return "Our number is: " + P + "/" + Q;
            }
        }
    }
}
